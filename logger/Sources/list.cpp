#include "list.hpp"

namespace tecom {
    
    list::list(): head(nullptr), tail(nullptr), count(0) { }
    
    uint16_t list::size() const {
        return count;
    }
    
    std::string list::get_head() const {
        return head ? head->value : "";
    }
    
    std::string list::get_tail() const {
        return tail ? tail->value : "";
    }
    
    void list::add_element(std::string value) {
        list_element *new_element = new list_element;
        
        new_element->value = value;
        new_element->next_element = nullptr;
        
        count++;
        
        if (!head) {
            head = new_element;
        } else {
            tail->next_element = new_element;
        }
        
        tail = new_element;
    }
    
    void list::remove_first() {
        if (!head) { return; }
        
        count--;
        
        list_element *link_to_next = head->next_element;
        
        delete head;
        
        head = link_to_next;
        
        if (!count) {
            tail = nullptr;
        }
    }
    
    list::~list() {
        while (head) {
            remove_first();
        }
    }
    
}
