#include "file_manager.hpp"
#include <fstream>
#include <chrono>

namespace tecom {
    
    using std::string;
    
    file_manager::file_manager(uint32_t size_per_file, uint32_t max_size) {
        if (size_per_file > max_size) {
            set_sizes(1024, 4096);
        } else {
            set_sizes(size_per_file, max_size);
        }
        
        this->current_file_count = 0;
    }
    
    void file_manager::create_file() {
        auto now_time = std::chrono::high_resolution_clock::now();
        auto sec = std::chrono::duration_cast<std::chrono::microseconds>(now_time.time_since_epoch());
        string file_name = std::to_string(sec.count()) + ".txt";

        created_files.add_element(file_name);
        
        std::ofstream file(file_name);
        file.close();
        
        current_file_count = 0;
    }
    
    void file_manager::write_to_file(const string & characters) {
        const uint32_t left_space_in_file = size_per_file - current_file_count;
        
        if (left_space_in_file >= characters.size()) {
            write_to_current_file(characters);
            return;
        }
        
        string characters_to_current_file = characters.substr(0, left_space_in_file);
        
        write_to_current_file(characters_to_current_file);
        
        string characters_to_other_files = characters.substr(left_space_in_file, characters.size());
        uint16_t characters_to_other_files_size = characters_to_other_files.size();
        
        while (characters_to_other_files_size >= size_per_file) {
            write_to_new_file(characters_to_other_files.substr(0, size_per_file));
            
            characters_to_other_files = characters_to_other_files.substr(size_per_file, characters_to_other_files_size);
            characters_to_other_files_size = characters_to_other_files.size();
        }
        
        if (characters_to_other_files.empty()) { return; }
        
        write_to_new_file(characters_to_other_files);
    }
    
    void file_manager::write_to_current_file(const string & characters) {
        if (!created_files.size()) {
            create_file();
        }
        
        check_available_space_by_adding(characters.size());
        
        string last_file_name = created_files.get_tail();
        
        std::ofstream file(last_file_name, std::ios_base::app);
        
        file << characters;
        
        current_file_count += characters.size();
        
        file.close();
    }
    
    void file_manager::delete_oldest_file() {
        string oldest_file_name = created_files.get_head();
        created_files.remove_first();
        remove(oldest_file_name.c_str());
    }
    
    void file_manager::write_to_new_file(const string & characters) {
        create_file();
        
        write_to_current_file(characters);
    }
    
    void file_manager::check_available_space_by_adding(const uint16_t & count) {
        if (actual_size() + count > max_size) {
            delete_oldest_file();
        }
    }
    
    uint32_t file_manager::actual_size() const {
        return size_per_file * (created_files.size() - 1) + current_file_count;
    }
    
    void file_manager::set_sizes(const uint32_t & size_per_file , const uint32_t & max_size) {
        this->size_per_file = size_per_file;
        this->max_size = max_size;
    }
    
}
