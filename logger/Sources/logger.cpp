#include "logger.hpp"
#include <fstream>

namespace tecom {
    
    logger::logger(const uint16_t & buffer_size, const uint32_t & size_per_file,  const uint32_t & max_size): file_manager(size_per_file, max_size) {
        buffer = std::string(buffer_size, '\0');
        
        current_buffer_count = 0;
    }
    
    logger::logger(const logger & copy) { }
    logger & logger::operator=(const logger & copy) { return *this; }

    const std::string & logger::get_buffer() const {
        return buffer;
    }
    
    void logger::trace(const std::string & characters) {
        uint16_t free_space_size = buffer.size() - current_buffer_count;
        
        std::string to_be_added = characters;
        
        while (to_be_added.size() >= free_space_size) {
            file_manager.write_to_file(
                buffer.substr(0, current_buffer_count) + to_be_added.substr(0, free_space_size)
            );
            
            to_be_added = to_be_added.substr(free_space_size, to_be_added.size());
            
            current_buffer_count = 0;
            buffer = std::string(buffer.size(), '\0');
            
            free_space_size = buffer.size() - current_buffer_count;
        }
        
        buffer.replace(current_buffer_count, to_be_added.size(), to_be_added);
        
        current_buffer_count += to_be_added.size();
    }
    
    logger::~logger() {
        file_manager.write_to_file(buffer.substr(0, current_buffer_count));
    }
    
}
