#include "logger.hpp"

int main(int argc, const char * argv[]) {
    tecom::logger *logger = new tecom::logger(10, -6, -10);
    
    logger->trace("First trace");
    logger->trace("Second trace");
    logger->trace("Third trace");
    
    delete logger;
    
    return 0;
}
