#ifndef logger_hpp
#define logger_hpp

#include "file_manager.hpp"

namespace tecom {
    class logger {
    private:
        logger(const logger &);
        logger & operator=(const logger &);
        
        uint16_t current_buffer_count;
        std::string buffer;
        file_manager file_manager;
    public:
        logger(const uint16_t & buffer_size = 512, const uint32_t & size_per_file = 1024,  const uint32_t & max_size = 4096);
       
        const std::string & get_buffer() const;
        
        void trace(const std::string &);
        
        ~logger();
    };
    
}

#endif
