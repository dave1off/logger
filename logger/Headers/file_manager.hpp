#ifndef file_manager_hpp
#define file_manager_hpp

#include "list.hpp"

namespace tecom {
    class file_manager {
    private:
        uint32_t size_per_file;
        uint32_t max_size;
        uint32_t current_file_count;
        list created_files;
        
        void write_to_current_file(const std::string &);
        void write_to_new_file(const std::string &);
        void check_available_space_by_adding(const uint16_t &);
        void set_sizes(const uint32_t &, const uint32_t &);
    public:
        file_manager(uint32_t size_per_file = 1024, uint32_t max_size = 4096);
        
        void create_file();
        void write_to_file(const std::string &);
        void delete_oldest_file();
        
        uint32_t actual_size() const;
    };
}

#endif
