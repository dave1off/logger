#ifndef list_hpp
#define list_hpp

#include <iostream>

namespace tecom {
    
    struct list_element {
        std::string value;
        list_element *next_element;
    };
    
    class list {
    private:
        list(const list &);
        list & operator=(const list &);
        
        list_element *head;
        list_element *tail;
        
        uint16_t count;
    public:
        list();
        
        uint16_t size() const;
        
        std::string get_head() const;
        std::string get_tail() const;
        
        void add_element(std::string);
        void remove_first();
        
        ~list();
    };
}

#endif
